
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Monthly Expense</div>

                <div class="panel-body">
                  <h1>Edit Monthly expense</h1>

                  {!! Form::model($expense, ['method' => 'PATCH', 'action' => ['ExpensesController@update', $expense->id]]) !!}

                    @include('partials.form', ['submitButtonText' => 'Update Expense'])

                  {!! Form::close() !!}

                      @include('errors.list')

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


