@extends('layouts.app')

@section('content')
	
	<div class="container">
		<h2>{{ $expense->id }} expense</h2>
	
		<a class="btn btn-info" href="{{ route('expenses.edit',$expense->id) }} ">Edit</a>

	<div  style="display: inline-block;">
		{{ Form::open(['route' => ['expenses.destroy', $expense->id], 'method' => 'DELETE' ]) }}

        <button type="submit" class="btn btn-danger">Delete</button>

      {{ Form::close() }} 
	</div>


	</div>

@endsection



