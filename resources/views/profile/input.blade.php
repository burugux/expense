<div class="form-group">

	{!! form::label('Name') !!}
	{!! form::text('name', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('E-Mail Address') !!}
	{!! form::text('email', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('Password') !!}
	{!! form::password('password', ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('Confirm Password') !!}
	{!! form::password('password', ['class' => 'form-control']) !!}

</div>

<!-- <div class="form-group">

	{!! form::label('Profile Image') !!}
	{!! form::file('image', null, ['class' => 'form-control']) !!}

</div> --> 

<div class="form-group">

	{!! form::label('Birthday') !!}
	{!! form::input('date', 'birthday', date('Y-m-d'), ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('Address') !!}
	{!! form::text('address', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('Phone Number') !!}
	{!! form::text('phoneNumber', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('city') !!}
	{!! form::text('city', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::label('County') !!}
	{!! form::text('county', null, ['class' => 'form-control']) !!}

</div>
	
<div class="form-group">

	{!! form::label('Income') !!}
	{!! form::text('income', null, ['class' => 'form-control']) !!}

</div>

<div class="form-group">

	{!! form::Submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}

</div>


