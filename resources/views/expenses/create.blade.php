
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Expense</div>

                <div class="panel-body">
                  <h1>Input new monthly expense</h1>

                  {!! Form::open(['url' => 'expenses']) !!}

                    @include('partials.form', ['submitButtonText' => 'Add Expense'])

                  {!! Form::close() !!}

                      @include('errors.list')

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
