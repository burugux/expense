<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Expense;

class ApiController extends Controller
{
    //

    protected $query;


    public function __construct(Expense $query)
    {
    	$this->query = $query;

    }

    public function index()
    {
    	$items = $this->query->all();

    	return response()->json($items);
    }

    /*public function store()
    {

    }
    */
}
