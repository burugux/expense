@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-body">
						<img src='uploads/avatars/{{Auth::user()->avatar}}' style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px; ">
						
						<h2> {{ Auth::user()->name }}'s profle</h2>
							
							
						<form enctype="multipart/form-data" action="/profile" method="POST">
                			<label>Update Profile Image</label>
                			<input type="file" name="avatar">
                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                			<input type="submit" name="submit" class="pull-right btn btn-sm btn-primary">
            			
            			</form> 	
            				<a href="/expenses"><button class="btn btn-primary">Home</button></a>



					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection